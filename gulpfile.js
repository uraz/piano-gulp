var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var fs = require('fs');
var path = require('path');
var Autolinker = require('autolinker');
var moment = require('moment');
var runSequence = require('run-sequence');
var ftp = require('vinyl-ftp');

moment.locale('pl');

gulp.task('default', function() {
    // Do nothing
    // gulp.watch(['app/data_{pl,en}/**/*.json'], ['update'])
});

gulp.task('clean', function() {
    var del = require('del');
    del.sync(['.tmp', 'dist/*'], {dot: true})
});

gulp.task('html-render', function() {

    var merged = require('merge-stream')();
    var languages = [
        {
            "id": "pl",
            "data": "app/data_pl/",
            "destination": ".tmp"
        },
        {
            "id": "en",
            "data": "app/data_en/",
            "destination": ".tmp/en"
        }
    ];
    
    languages.forEach(function(language, index, array) {
        language.return = gulp.src(['app/templates/pages/*.html'])
            .pipe(plugins.data(function(file) { 
                var data_file, json_data, json_data_part;

                json_data = {};
                json_data_part = {};

                data_file = language.data + 'pages/' + path.basename(file.path, '.html') + '.json';

                if (fs.existsSync(data_file)) {
                    json_data = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                }

                /**
                 * Quick and dirty....
                 * Load footer data and inject it into global json data object
                 */
                data_file = language.data + 'partials/footer.json';

                if (fs.existsSync(data_file)) {
                    json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                    json_data.sections.footer = json_data_part.footer;
                }

                /**
                 * Quick and dirty....
                 * Load navigation data and inject it into global json data object
                 */
                data_file = language.data + 'partials/navigation.json';

                if (fs.existsSync(data_file)) {
                    json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                    json_data.sections.header.navigation.main = json_data_part.main;
                    json_data.sections.footer.navigation.secondary = json_data_part.secondary;
                }

                /**
                 * Quick and dirty....
                 * Load footer data and inject it into global json data object
                 */
                data_file = language.data + 'facebook/photos.json';

                if (fs.existsSync(data_file)) {
                    json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                    // var i = json_data.sections.footer.ad.display ? 4 : 12;
                    var i = 12;
                    json_data.sections.footer.photos = json_data_part.slice(0, i);
                }

                /**
                 * Quick and dirty....
                 * Load footer data and inject it into global json data object
                 */
                data_file = language.data + 'facebook/posts.json';
                var autolinker = new Autolinker({
                    'newWindow': false,
                    'email': false,
                    'phone': false,
                    'twitter': false
                });

                if (fs.existsSync(data_file)) {
                    json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                    json_data_part = json_data_part.slice(0, 2);

                    json_data_part.forEach(function(item) {
                        item.created_time_formatted = moment(item.created_time).format('LLL');

                        item.message_formatted = item.message.replace(/(?:\r\n|\r|\n)/g, '<br>');
                        item.message_formatted = autolinker.link(item.message_formatted);
                    });

                    json_data.sections.footer.posts = json_data_part;
                }

                if (path.basename(file.path, '.html') == 'restaurant') {
                    data_file = language.data + 'wp/restaurant.json';
                    if (fs.existsSync(data_file)) {
                        json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));
                        json_data.wp = json_data_part;
                    }
                }
                
                if (path.basename(file.path, '.html') == 'index') {

                    data_file = language.data + 'wp/banners.json';

                    if (fs.existsSync(data_file)) {
                        json_data_part = JSON.parse(fs.readFileSync(data_file, 'utf8'));

                        if (Array.isArray(json_data_part) && json_data_part.length) {
                            var banner;
                            var image = {};
                            var banners = [];

                            for (i = 0, len = json_data_part.length; i < len; i++) {
                                banner = {
                                    'status': json_data_part[i]['acf']['banner__status'],
                                    'description': json_data_part[i]['acf']['banner__description'],
                                    'button_label': json_data_part[i]['acf']['banner__button-label'],
                                    'image': {
                                        'src': json_data_part[i]['acf']['banner__image']['sizes']['banner-image'],
                                        'width': json_data_part[i]['acf']['banner__image']['sizes']['banner-image-width'],
                                        'height': json_data_part[i]['acf']['banner__image']['sizes']['banner-image-height']
                                    }
                                };

                                // Add only banners with status set to visible
                                if (banner.status) {
                                    banners.push(banner);
                                }
                            }

                            json_data.sections.header.banners = banners;
                        }
                    }
                }

                // Add language code
                json_data.language = language.id;

                return json_data;
             }))
            .pipe(plugins.nunjucksRender({'path': 'app/templates'}))
            .pipe(gulp.dest(language.destination));
        
        merged.add(language.return);
    });
    
    return merged;
});

gulp.task('html-parse', function() {
    // All available options 
    // https://github.com/kangax/html-minifier
    var htmlOptions = { 
        'removeComments': true,
        'collapseWhitespace': true
    };
    
    return gulp.src('.tmp/**/*.html')
        .pipe(plugins.useref({ searchPath: ['.tmp','app'] }))
        .pipe(plugins.ignore.exclude(['*.js', '*.css']))
        .pipe(plugins.htmlmin(htmlOptions))
        .pipe(gulp.dest('dist'));
});

gulp.task('ftp', function() {
    var connection = ftp.create({
        host: 'ria.megiteam.pl',
        user: 'ria_piano',
        pass: 'yL9GscfPikqK'
    });
    
    // var connection = ftp.create({
    //     host: 'ria.megiteam.pl',
    //     user: 'ria_piano_staging',
    //     pass: 'udUHp59bQ4Mj'
    // });
    
    var globs = [
        'dist/**/*.html'
    ];
    
    return gulp.src(globs, { 'base': 'dist', 'buffer': false })
        .pipe(connection.dest('/'));
});

gulp.task('html', ['clean'], function(callback) {
    runSequence('html-render', 'html-parse', callback);
});

gulp.task('deploy', function(callback) {
    runSequence('html', 'ftp', callback);
});